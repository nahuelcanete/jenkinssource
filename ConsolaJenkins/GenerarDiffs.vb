﻿Imports System.IO
Module GenerarDiffs
    Private myXmlFilePath = "Config\xmlUserData.xml"

    Sub Main()

        'creamos las carpetas en el caso que no existan. En procesos vamos a guardar la app que ejecuta
        Call GenerarDiffs.crear_carpeta("Config")
        Call GenerarDiffs.crear_carpeta("Procesos")

        If My.Computer.FileSystem.FileExists(myXmlFilePath) = True Then

            'declaramos variables
            Dim xRead As System.IO.StreamReader
            Dim clArgs() As String = Environment.GetCommandLineArgs()
            Dim vWorkinCopy As String = String.Empty
            Dim vIDProc As String = String.Empty
            Dim i As Integer = 1
            Dim vLine As String = ""
            Dim vSVN As String = ""
            Dim vLOG As String = ""
            Dim vFecha As String = Format(Now(), "yyyyMMdd_hhmmss")

            vWorkinCopy = clArgs(1)
            vIDProc = clArgs(2)

            xRead = File.OpenText(myXmlFilePath)

            'leemos el archivo de configuracion linea a linea y el objetivo es capturar svn.exe y el path de logueo.
            Do Until xRead.EndOfStream

                vLine = xRead.ReadLine

                If vLine.Contains("SVN_PATH") Then
                    vSVN = Replace(Replace(Replace(vLine, "<SVN_PATH>", ""), vbTab, ""), "</SVN_PATH>", "")
                End If

                If vLine.Contains("LOG_PATH") Then
                    vLOG = Replace(Replace(Replace(vLine, "<LOG_PATH>", ""), vbTab, ""), "</LOG_PATH>", "") &
                           "Log_" & vIDProc & "_" & vFecha & ".txt"
                End If

                i = i + 1
            Loop

            xRead.Close()

            'generamos el bat y lo ejecutamos
            Call GenerarDiffs.crear_bat(vSVN, vWorkinCopy, vLOG, vIDProc)
        End If
    End Sub
    Sub abrir_programa(ByVal file_path As String)

        Console.WriteLine("rutina de abrir programa")
        Process.Start(file_path)

    End Sub
    Sub crear_bat(ByVal svn As String, ByVal repo As String, ByVal log As String, ByVal ID As String)
        Dim sb As New System.Text.StringBuilder
        Dim vSentencia As String

        vSentencia = "svn.exe diff " & """" & repo & """" & ">>" & """" & log & """"
        Console.WriteLine(vSentencia)

        Dim vUnidad As String = Left(svn, 1)
        Dim vSvnPath As String = Left(svn, InStrRev(svn, "\"))

        sb.AppendLine("::Hacemos el cambio de unidad")
        sb.AppendLine(vUnidad & ":")
        sb.AppendLine("::Hacemos el cd del path")
        sb.AppendLine("CD " & """" & vSvnPath & """")
        sb.AppendLine("::Hacemos el cd del path")
        sb.AppendLine(vSentencia)

        'guardamos el archivo
        IO.File.WriteAllText("Procesos\proc_" & ID & ".bat", sb.ToString())

        'ejecutamos el programa
        Call GenerarDiffs.abrir_programa("Procesos\proc_" & ID & ".bat")
    End Sub
    Sub crear_carpeta(ByVal directorio As String)
        If (Not System.IO.Directory.Exists(directorio)) Then
            System.IO.Directory.CreateDirectory(directorio)
        End If
    End Sub
End Module
