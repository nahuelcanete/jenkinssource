﻿Public Class Form1
    Private myXmlFilePath = "Config\xmlUserData.xml"
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles Me.Load
        If My.Computer.FileSystem.FileExists(myXmlFilePath) = True Then
            DsUsers.ReadXml(myXmlFilePath)
        End If
    End Sub
    Private Sub DtUsersBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles DtUsersBindingNavigatorSaveItem.Click
        Me.Validate()
        DtUsersBindingSource.EndEdit()
        Try
            lblStatus.Text = ""
            DsUsers.WriteXml(myXmlFilePath)
            lblStatus.Text = "Records Saved Successfully"
        Catch ex As Exception
            lblStatus.Text = "Something Went Wrong. Error: " & ex.ToString
        End Try


    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If OpenFileDialog1.ShowDialog = DialogResult.OK Then
            'OpenFileDialog1.Filter = "Text File [*.txt]|*.txt|All Files [*.*]|*.*"
            'OpenFileDialog1.CheckFileExists = True
            'OpenFileDialog1.Title = "OpenFile"
            SVN_PATHTextBox.Text = OpenFileDialog1.FileName
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If FolderBrowserDialog1.ShowDialog = DialogResult.OK Then
            txtWorkingCopy.Text = FolderBrowserDialog1.SelectedPath
        End If
    End Sub


End Class
