﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim UserIDLabel As System.Windows.Forms.Label
        Dim FirstNameLabel As System.Windows.Forms.Label
        Dim LastNameLabel As System.Windows.Forms.Label
        Dim GamerTagLabel As System.Windows.Forms.Label
        Dim SVN_PATHLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.DtUsersBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.DtUsersBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DsUsers = New SavingDatatoXml.dsUsers()
        Me.DtUsersBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.UserIDTextBox = New System.Windows.Forms.TextBox()
        Me.FirstNameTextBox = New System.Windows.Forms.TextBox()
        Me.LastNameTextBox = New System.Windows.Forms.TextBox()
        Me.GamerTagTextBox = New System.Windows.Forms.TextBox()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.txtWorkingCopy = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.SVN_PATHTextBox = New System.Windows.Forms.TextBox()
        UserIDLabel = New System.Windows.Forms.Label()
        FirstNameLabel = New System.Windows.Forms.Label()
        LastNameLabel = New System.Windows.Forms.Label()
        GamerTagLabel = New System.Windows.Forms.Label()
        SVN_PATHLabel = New System.Windows.Forms.Label()
        CType(Me.DtUsersBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.DtUsersBindingNavigator.SuspendLayout()
        CType(Me.DtUsersBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsUsers, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'UserIDLabel
        '
        UserIDLabel.AutoSize = True
        UserIDLabel.Location = New System.Drawing.Point(21, 43)
        UserIDLabel.Name = "UserIDLabel"
        UserIDLabel.Size = New System.Drawing.Size(46, 13)
        UserIDLabel.TabIndex = 1
        UserIDLabel.Text = "User ID:"
        '
        'FirstNameLabel
        '
        FirstNameLabel.AutoSize = True
        FirstNameLabel.Location = New System.Drawing.Point(21, 69)
        FirstNameLabel.Name = "FirstNameLabel"
        FirstNameLabel.Size = New System.Drawing.Size(60, 13)
        FirstNameLabel.TabIndex = 3
        FirstNameLabel.Text = "First Name:"
        '
        'LastNameLabel
        '
        LastNameLabel.AutoSize = True
        LastNameLabel.Location = New System.Drawing.Point(21, 95)
        LastNameLabel.Name = "LastNameLabel"
        LastNameLabel.Size = New System.Drawing.Size(61, 13)
        LastNameLabel.TabIndex = 5
        LastNameLabel.Text = "Last Name:"
        '
        'GamerTagLabel
        '
        GamerTagLabel.AutoSize = True
        GamerTagLabel.Location = New System.Drawing.Point(21, 121)
        GamerTagLabel.Name = "GamerTagLabel"
        GamerTagLabel.Size = New System.Drawing.Size(63, 13)
        GamerTagLabel.TabIndex = 7
        GamerTagLabel.Text = "Gamer Tag:"
        '
        'SVN_PATHLabel
        '
        SVN_PATHLabel.AutoSize = True
        SVN_PATHLabel.Location = New System.Drawing.Point(21, 180)
        SVN_PATHLabel.Name = "SVN_PATHLabel"
        SVN_PATHLabel.Size = New System.Drawing.Size(64, 13)
        SVN_PATHLabel.TabIndex = 14
        SVN_PATHLabel.Text = "SVN PATH:"
        '
        'DtUsersBindingNavigator
        '
        Me.DtUsersBindingNavigator.AddNewItem = Nothing
        Me.DtUsersBindingNavigator.BindingSource = Me.DtUsersBindingSource
        Me.DtUsersBindingNavigator.CountItem = Nothing
        Me.DtUsersBindingNavigator.DeleteItem = Nothing
        Me.DtUsersBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DtUsersBindingNavigatorSaveItem})
        Me.DtUsersBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.DtUsersBindingNavigator.MoveFirstItem = Nothing
        Me.DtUsersBindingNavigator.MoveLastItem = Nothing
        Me.DtUsersBindingNavigator.MoveNextItem = Nothing
        Me.DtUsersBindingNavigator.MovePreviousItem = Nothing
        Me.DtUsersBindingNavigator.Name = "DtUsersBindingNavigator"
        Me.DtUsersBindingNavigator.PositionItem = Nothing
        Me.DtUsersBindingNavigator.Size = New System.Drawing.Size(729, 25)
        Me.DtUsersBindingNavigator.TabIndex = 0
        Me.DtUsersBindingNavigator.Text = "BindingNavigator1"
        '
        'DtUsersBindingSource
        '
        Me.DtUsersBindingSource.DataMember = "dtUsers"
        Me.DtUsersBindingSource.DataSource = Me.DsUsers
        '
        'DsUsers
        '
        Me.DsUsers.DataSetName = "dsUsers"
        Me.DsUsers.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DtUsersBindingNavigatorSaveItem
        '
        Me.DtUsersBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.DtUsersBindingNavigatorSaveItem.Image = CType(resources.GetObject("DtUsersBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.DtUsersBindingNavigatorSaveItem.Name = "DtUsersBindingNavigatorSaveItem"
        Me.DtUsersBindingNavigatorSaveItem.Size = New System.Drawing.Size(23, 22)
        Me.DtUsersBindingNavigatorSaveItem.Text = "Guardar datos"
        '
        'UserIDTextBox
        '
        Me.UserIDTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DtUsersBindingSource, "UserID", True))
        Me.UserIDTextBox.Location = New System.Drawing.Point(90, 40)
        Me.UserIDTextBox.Name = "UserIDTextBox"
        Me.UserIDTextBox.Size = New System.Drawing.Size(100, 20)
        Me.UserIDTextBox.TabIndex = 2
        '
        'FirstNameTextBox
        '
        Me.FirstNameTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DtUsersBindingSource, "FirstName", True))
        Me.FirstNameTextBox.Location = New System.Drawing.Point(90, 66)
        Me.FirstNameTextBox.Name = "FirstNameTextBox"
        Me.FirstNameTextBox.Size = New System.Drawing.Size(100, 20)
        Me.FirstNameTextBox.TabIndex = 4
        '
        'LastNameTextBox
        '
        Me.LastNameTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DtUsersBindingSource, "LastName", True))
        Me.LastNameTextBox.Location = New System.Drawing.Point(90, 92)
        Me.LastNameTextBox.Name = "LastNameTextBox"
        Me.LastNameTextBox.Size = New System.Drawing.Size(100, 20)
        Me.LastNameTextBox.TabIndex = 6
        '
        'GamerTagTextBox
        '
        Me.GamerTagTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DtUsersBindingSource, "GamerTag", True))
        Me.GamerTagTextBox.Location = New System.Drawing.Point(90, 118)
        Me.GamerTagTextBox.Name = "GamerTagTextBox"
        Me.GamerTagTextBox.Size = New System.Drawing.Size(100, 20)
        Me.GamerTagTextBox.TabIndex = 8
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.ForeColor = System.Drawing.Color.LightGreen
        Me.lblStatus.Location = New System.Drawing.Point(21, 211)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(0, 13)
        Me.lblStatus.TabIndex = 9
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.InitialDirectory = "C:\"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(635, 170)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(82, 23)
        Me.Button1.TabIndex = 11
        Me.Button1.Text = "Select File"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'txtWorkingCopy
        '
        Me.txtWorkingCopy.Location = New System.Drawing.Point(91, 215)
        Me.txtWorkingCopy.Name = "txtWorkingCopy"
        Me.txtWorkingCopy.Size = New System.Drawing.Size(538, 20)
        Me.txtWorkingCopy.TabIndex = 12
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(635, 209)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(82, 26)
        Me.Button2.TabIndex = 13
        Me.Button2.Text = "Select Folder"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'SVN_PATHTextBox
        '
        Me.SVN_PATHTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DtUsersBindingSource, "SVN_PATH", True))
        Me.SVN_PATHTextBox.Location = New System.Drawing.Point(91, 173)
        Me.SVN_PATHTextBox.Name = "SVN_PATHTextBox"
        Me.SVN_PATHTextBox.Size = New System.Drawing.Size(538, 20)
        Me.SVN_PATHTextBox.TabIndex = 15
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(729, 369)
        Me.Controls.Add(SVN_PATHLabel)
        Me.Controls.Add(Me.SVN_PATHTextBox)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.txtWorkingCopy)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(UserIDLabel)
        Me.Controls.Add(Me.UserIDTextBox)
        Me.Controls.Add(FirstNameLabel)
        Me.Controls.Add(Me.FirstNameTextBox)
        Me.Controls.Add(LastNameLabel)
        Me.Controls.Add(Me.LastNameTextBox)
        Me.Controls.Add(GamerTagLabel)
        Me.Controls.Add(Me.GamerTagTextBox)
        Me.Controls.Add(Me.DtUsersBindingNavigator)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.DtUsersBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.DtUsersBindingNavigator.ResumeLayout(False)
        Me.DtUsersBindingNavigator.PerformLayout()
        CType(Me.DtUsersBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsUsers, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents DsUsers As dsUsers
    Friend WithEvents DtUsersBindingSource As BindingSource
    Friend WithEvents DtUsersBindingNavigator As BindingNavigator
    Friend WithEvents DtUsersBindingNavigatorSaveItem As ToolStripButton
    Friend WithEvents UserIDTextBox As TextBox
    Friend WithEvents FirstNameTextBox As TextBox
    Friend WithEvents LastNameTextBox As TextBox
    Friend WithEvents GamerTagTextBox As TextBox
    Friend WithEvents lblStatus As Label
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents Button1 As Button
    Friend WithEvents txtWorkingCopy As TextBox
    Friend WithEvents Button2 As Button
    Friend WithEvents FolderBrowserDialog1 As FolderBrowserDialog
    Friend WithEvents SVN_PATHTextBox As TextBox
End Class
